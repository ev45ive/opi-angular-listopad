import {
  Directive,
  ElementRef,
  Input,
  OnInit,
  SimpleChanges,
  Renderer2,
  HostBinding,
  HostListener
} from "@angular/core";

@Directive({
  selector: "[appHighlight]"
  // host: {
  //   "(mouseenter)": "activate($event)",
  //   "[style.color]": "color"
  // }
})
export class HighlightDirective /* implements OnInit */ {
  @Input("appHighlight")
  appHighlight;

  @HostBinding("style.color")
  get color() {
    return this.active ? this.appHighlight : "";
  }

  active = false;

  @HostListener("mouseenter", ["$event.x", "$event.y"])
  activate(x: number, y: number) {
    // this.color = this.appHighlight;
    this.active = true;
  }

  @HostListener("mouseleave", ["$event.x", "$event.y"])
  deactivate(x: number, y: number) {
    // this.color = "";
    this.active = false;
  }

  constructor() {}

  /*   ngOnInit() {
    console.log("ngOnInit", this.color);
    // this.renderer.setStyle(this.elem.nativeElement, "color", this.color);
    // this.unlisten = this.renderer.listen(this.elem.nativeElement,'mouseenter',()=>{})
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("ngOnChanges", changes);
    // this.renderer.setStyle(this.elem.nativeElement, "color", this.color);
  }

  ngDoCheck() {
    console.log("ngDoCheck");
  }

  ngOnDestroy() {
    console.log("ngOnDestroy");
    // this.unlisten()
  } */
}

// console.log(HighlightDirective)