import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "shorten",
  // pure: false
})
export class ShortenPipe implements PipeTransform {

  constructor(){}

  transform(value: string, maxLen = 20): any {

    // console.log('znowu')

    return value.length >= maxLen ? value.substr(0, maxLen) + "..." : value;
  }
}
