import { Component, OnInit, Input, Optional, EventEmitter } from '@angular/core';

@Component({
  selector: "app-tab",
  templateUrl: "./tab.component.html",
  styleUrls: ["./tab.component.css"]
})
export class TabComponent implements OnInit {
  @Input()
  title;

  open = false;

  toggleChange = new EventEmitter()

  toggle() {
    this.toggleChange.emit(this)
  }

  constructor() {}

  ngOnInit() {}
}
