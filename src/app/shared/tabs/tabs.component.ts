import { Component, OnInit, ViewChild, ContentChildren, QueryList } from "@angular/core";
import { TabComponent } from "../tab/tab.component";
import { TabsNavComponent } from "../tabs-nav/tabs-nav.component";

@Component({
  selector: "app-tabs",
  templateUrl: "./tabs.component.html",
  styleUrls: ["./tabs.component.css"]
})
export class TabsComponent implements OnInit {
  @ViewChild(TabsNavComponent)
  navRef: TabsNavComponent;

  @ContentChildren(TabComponent, { descendants: true })
  tablist: QueryList<TabComponent>;

  toggle(tab) {
    this.tablist.forEach(tab => (tab.open = false));
    tab.open = !tab.open;
  }

  constructor() {}

  ngOnInit() {
    // set before child checks
    // this.navRef.links = this.tablist
    this.navRef.toggleChange.subscribe(tab => {
      this.toggle(tab);
    });
  }

  ngAfterContentInit(){
    console.log(this.tablist)
    this.tablist.forEach(tab => {
      tab.toggleChange.subscribe(tab => this.toggle(tab))
    })
  }

  ngAfterViewInit() {
    // set after child checks
    setTimeout(() => {
      this.navRef.links = this.tablist.toArray();
    });
    console.log(this.navRef);
  }
}
