import {
  Directive,
  ContentChild,
  ContentChildren,
  QueryList
} from "@angular/core";
import { AppTabDirective } from "./app-tab.directive";

@Directive({
  selector: "[appTabs]"
})
export class AppTabsDirective {
  @ContentChildren(AppTabDirective)
  tabs: QueryList<AppTabDirective>;

  constructor() {}

  ngAfterContentInit(){
    this.tabs.forEach(tab => tab.show())
  }
}
