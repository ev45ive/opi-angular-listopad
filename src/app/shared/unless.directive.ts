import { PlaylistsViewComponent } from "../playlists/components/playlists-view/playlists-view.component";
import {
  Directive,
  TemplateRef,
  ViewContainerRef,
  ComponentFactoryResolver,
  Input,
  ViewRef
} from "@angular/core";

@Directive({
  selector: "[appUnless]"
})
export class UnlessDirective {

  viewCache: ViewRef

  @Input()
  set appUnless(hide){
    if(hide){
        // this.vcr.clear()
       this.viewCache = this.vcr.detach(0)

    }else{

      if(this.viewCache){
        this.vcr.insert(this.viewCache,0)
      }else{
        this.vcr.createEmbeddedView(this.tpl, {
          message: "ala ma kota",
          $implicit: "lubie placki!"
        },0);
      }
  
    }
  }

  constructor(
    private tpl: TemplateRef<any>, 
    private vcr: ViewContainerRef
    ) {

  }
}
