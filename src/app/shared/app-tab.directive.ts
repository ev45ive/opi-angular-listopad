import { Directive, Input, TemplateRef, ViewContainerRef } from "@angular/core";

@Directive({
  selector: "[appTab]"
})
export class AppTabDirective {
  @Input()
  appTabTitle: string;

  show(){
    this.vcr.createEmbeddedView(this.tpl)
  }

  constructor(private tpl: TemplateRef<any>, private vcr: ViewContainerRef) {
  }
}
