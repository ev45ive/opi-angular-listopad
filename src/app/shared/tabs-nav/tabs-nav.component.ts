import { Component, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tabs-nav',
  templateUrl: './tabs-nav.component.html',
  styleUrls: ['./tabs-nav.component.css']
})
export class TabsNavComponent implements OnInit {

  links = []

  toggleChange = new EventEmitter()

  toggle(tab){
    this.toggleChange.emit(tab)
  }

  constructor() {
  }
  
  ngOnInit() {
    
  }

}
