import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HighlightDirective } from "./highlight.directive";
import { UnlessDirective } from './unless.directive';
import { CardComponent } from './card/card.component';
import { TabsComponent } from './tabs/tabs.component';
import { TabComponent } from './tab/tab.component';
import { TabsNavComponent } from './tabs-nav/tabs-nav.component';
import { AppTabsDirective } from './app-tabs.directive';
import { AppTabDirective } from './app-tab.directive';
import { MessageComponent } from './message/message.component';
import { ShortenPipe } from './shorten.pipe';

@NgModule({
  declarations: [HighlightDirective, UnlessDirective, CardComponent, TabsComponent, TabComponent, TabsNavComponent, AppTabsDirective, AppTabDirective, MessageComponent, ShortenPipe],
  imports: [CommonModule],
  exports: [HighlightDirective, UnlessDirective, CardComponent, TabsComponent, TabComponent, TabsNavComponent, AppTabsDirective, AppTabDirective, ShortenPipe]
})
export class SharedModule {}
