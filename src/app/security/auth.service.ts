import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";

export class AuthConfig {
  auth_url: string;
  client_id: string;
  response_type: string;
  redirect_uri: string;
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(private config: AuthConfig) {}

  authorize() {
    const { client_id, redirect_uri, response_type, auth_url } = this.config;

    const params = new HttpParams({
      fromObject: {
        client_id,
        redirect_uri,
        response_type
      }
    });

    const url = `${auth_url}?${params.toString()}`;

    sessionStorage.removeItem("token");
    location.href = url;
  }

  token = "";

  getToken() {
    this.token = JSON.parse(sessionStorage.getItem("token"));

    if (!this.token && window.location.hash) {
      const params = new HttpParams({
        fromString: window.location.hash
      });
      this.token = params.get("#access_token");
      sessionStorage.setItem("token", JSON.stringify(this.token));
    }

    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
