import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { environment } from "src/environments/environment";
import { AuthConfig, AuthService } from "./auth.service";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptorService } from "./auth-interceptor.service";
import { ModuleWithProviders } from "@angular/compiler/src/core";

@NgModule({
  declarations: [],
  imports: [CommonModule]
  /*  providers: [
    {
      provide: AuthConfig,
      useValue: environment.authConfig
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ] */
})
export class SecurityModule {
  constructor(private auth: AuthService) {
    this.auth.getToken();
  }

  static forChild(): ModuleWithProviders {
    return {
      ngModule: SecurityModule,
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useExisting: AuthInterceptorService,
          multi: true
        }
      ]
    };
  }

  static forRoot(config: AuthConfig): ModuleWithProviders {
    /*  */
    return {
      ngModule: SecurityModule,
      providers: [
        {
          provide: AuthConfig,
          useValue: config
        },
        AuthInterceptorService,
        {
          provide: HTTP_INTERCEPTORS,
          useExisting: AuthInterceptorService,
          multi: true
        }
      ]
    };
  }
}
