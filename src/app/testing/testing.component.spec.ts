import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from "@angular/core/testing";

import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";

import { TestingComponent } from "./testing.component";
import { By } from "@angular/platform-browser";

fdescribe("TestingComponent", () => {
  let fixture: ComponentFixture<TestingComponent>;
  let instance: TestingComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestingComponent],
      imports: [HttpClientTestingModule],
      providers: []
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    instance = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(instance).toBeTruthy();
  });

  // it("should render message", () => {
  //   expect(fixture.debugElement.nativeElement.innerHTML).toMatch(
  //     "testing works!"
  //   );
  // });

  it("should render message", () => {
    const elem = fixture.debugElement.query(By.css("p"));
    expect(elem.nativeElement.innerHTML).toMatch("testing works!");
  });

  it("should update message", () => {
    instance.message = "placki";
    fixture.detectChanges();

    const elem = fixture.debugElement.query(By.css("p"));
    expect(elem.nativeElement.innerHTML).toMatch("placki");
  });

  it("http", inject(
    [HttpTestingController],
    (ctrl: HttpTestingController) => {

      instance.requestPlacki()
      ctrl.expectOne('placki','plackiservice').flush(
        'placki'
      )

    }
  ));
});
