import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-testing",
  templateUrl: "./testing.component.html",
  styleUrls: ["./testing.component.css"]
})
export class TestingComponent implements OnInit {
  requestPlacki(): any {
    this.http.get('placki').subscribe((resp)=>{
      console.log('resp',resp)
    })
  }

  message = "testing works!";

  constructor(private http: HttpClient) {}

  ngOnInit() {}
}
