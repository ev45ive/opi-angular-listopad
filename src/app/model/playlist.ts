import { Track } from "./Track";

// class Playlist implements IPlaylist{}

export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  /**
   * HEX Color
   */
  color: string;
  // tracks: Array<Track>;
  tracks?: Track[];
}

