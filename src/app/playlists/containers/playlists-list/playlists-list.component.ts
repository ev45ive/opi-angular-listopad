import { Component, OnInit } from "@angular/core";
import { PlaylistsService } from "../../services/playlists.service";
import { Playlist } from "../../../model/playlist";
import { Router, ActivatedRoute } from "@angular/router";
import { map, switchMap, tap, pluck } from 'rxjs/operators';

@Component({
  template: `
    <app-items-list
      [items]="playlists$ | async"
      [selected]="selected$ | async"
      (selectedChange)="select($event)"
    >
    </app-items-list>
  `
})
export class PlaylistsListComponent implements OnInit {
  playlists$ = this.service.getPlaylists();

  selected$ =  this.route.data.pipe(
    pluck<any,Playlist>("playlist")
  );

  constructor(
    private service: PlaylistsService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  select(playlist: Playlist) {
    this.router.navigate(["/playlists", playlist.id], {
      replaceUrl: true
    });
  }

  ngOnInit() {}
}
