import { Component, OnInit } from "@angular/core";
import { PlaylistsService } from "../../services/playlists.service";
import { ActivatedRoute, Data } from "@angular/router";
import { Observable } from "rxjs";
import { Playlist } from "src/app/model/playlist";
import { map, switchMap, pluck } from "rxjs/operators";

@Component({
  selector: "app-selected-playlist",
  template: `
    <ng-container *ngIf="(playlist$ | async) as playlist">
      <app-playlist-details [playlist]="playlist"> </app-playlist-details>
    </ng-container>
  `
})
export class SelectedPlaylistComponent implements OnInit {
  // playlist$ = this.route.paramMap.pipe(
  //   map(paramMap => paramMap.get("id")),
  //   map(id => parseInt(id, 10)),
  //   switchMap(id => this.service.getPlaylist(id))
  // );

  playlist$ = this.route.data.pipe(
    pluck<any,Playlist>("playlist")
  );

  constructor(
    private service: PlaylistsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {}
}
