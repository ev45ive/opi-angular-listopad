import { Injectable } from "@angular/core";
import { Observable, BehaviorSubject, of } from "rxjs";
import { Playlist } from "../../model/playlist";
import { DeprecatedDatePipe } from "@angular/common";

@Injectable({
  providedIn: "root"
})
export class PlaylistsService {
  private playlists = new BehaviorSubject<Playlist[]>([
    {
      id: 123,
      name: "Angular Hits",
      favourite: false,
      color: "#ff0000"
    },
    {
      id: 234,
      name: "Angular TOP20",
      favourite: true,
      color: "#00ff00"
    },
    {
      id: 345,
      name: "BEST of Angular",
      favourite: false,
      color: "#0000ff"
    }
  ]);

  constructor() {}

  getPlaylists(): Observable<Playlist[]> {
    return this.playlists.asObservable();
  }

  getPlaylist(id): Observable<Playlist> {
    return of(this.playlists.getValue().find(p => p.id === id));
  }

  savePlaylist(draft: Playlist): Observable<Playlist> {
    const playlists = this.playlists.getValue().map(p => {
      return p.id == draft.id ? draft : p;
    });
    this.playlists.next(playlists);
    return of(draft);
  }
}
