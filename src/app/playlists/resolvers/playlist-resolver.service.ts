import { Injectable } from "@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { Playlist } from "src/app/model/playlist";
import { Observable } from "rxjs";
import { PlaylistsService } from "../services/playlists.service";

@Injectable({
  providedIn: "root"
})
export class PlaylistResolverService implements Resolve<Playlist> {
 
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    let id = parseInt(route.paramMap.get("id"));

    return this.service.getPlaylist(id);
  }

  constructor(private service: PlaylistsService) {}

}
