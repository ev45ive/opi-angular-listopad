import { Component, OnInit, ViewEncapsulation, Input, EventEmitter, Output } from "@angular/core";
import { Playlist } from "src/app/model/playlist";
import { NgForOfContext, NgForOf } from "@angular/common";

NgForOfContext;
NgForOf;

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.css"]
  // inputs:['playlists:items']
})
export class ItemsListComponent implements OnInit {

  hover:Playlist

  @Input('items') 
  playlists: Playlist[] = [];

  @Input()
  selected: Playlist;

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(playlist){
    this.selectedChange.emit(playlist)
  }

  constructor() {}

  trackFn(index: number, item: Playlist) {
    return item.id;
  }

  ngOnInit() {}
}
