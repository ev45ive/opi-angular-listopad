import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { Playlist } from "src/app/model/playlist";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-playlist-details",
  templateUrl: "./playlist-details.component.html",
  styleUrls: ["./playlist-details.component.css"]
})
export class PlaylistDetailsComponent implements OnInit {
  @Input()
  playlist: Playlist;

  mode: "show" | "edit" = "show";

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }

  @Output()
  playlistChange = new EventEmitter<Playlist>();

  save(formRef: NgForm) {
    
    const draft: Pick<Playlist, "name" | "favourite" | "color"> = formRef.value;

    const playlist = {
      ...this.playlist,
      ...draft
    };
    this.playlistChange.emit(playlist);

    // Destroys form with all ngModels !!!:
    this.mode = "show";
  }

  //https://almerosteyn.com/2016/04/linkup-custom-control-to-ngcontrol-ngmodel

  constructor() {}

  ngOnInit() {}
}


// https://pl.wikipedia.org/wiki/Metoda_gumowej_kaczuszki