import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import {
  AbstractControl,
  FormControl,
  FormArray,
  FormGroup,
  FormBuilder,
  Validators,
  ValidatorFn,
  ValidationErrors,
  AsyncValidatorFn
} from "@angular/forms";
import {
  distinctUntilKeyChanged,
  distinctUntilChanged,
  filter,
  debounceTime,
  combineLatest,
  withLatestFrom,
  tap
} from "rxjs/operators";
import { Observable, Observer } from "rxjs";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.css"]
})
export class SearchFormComponent implements OnInit {
  queryForm: FormGroup;

  @Input()
  set query(q) {
    this.queryForm.get("query").setValue(q, {
      emitEvent: false
    });
  }

  constructor(private bob: FormBuilder) {
    const censor = (badword: string): ValidatorFn =>
      //
      (control: AbstractControl): ValidationErrors | null => {
        const hasError =
          typeof control.value == "string" && control.value.includes(badword);

        return hasError
          ? {
              censor: { badword: badword }
            }
          : null;
      };

    // https://jsfiddle.net/Lbpxc0y7/

    const asyncCensor = (badword: string): AsyncValidatorFn => (
      control: AbstractControl
    ): Observable<ValidationErrors | null> => {
      // return this.http.post('validate',{value}).pipe(debounceTime(),map(resp=>errors))

      return Observable.create(
        (observer: Observer<ValidationErrors | null>) => {
          const handler = setTimeout(() => {
            const hasError =
              typeof control.value == "string" &&
              control.value.includes(badword);
            const result = hasError ? { censor: { badword: badword } } : null;

            observer.next(result);
            observer.complete();
          }, 1000);

          return () => {
            clearTimeout(handler);
          };
        }
      );
    };

    this.queryForm = this.bob.group({
      query: new FormControl(
        "",
        [
          Validators.required,
          Validators.minLength(3)
          // censor("batman")
        ],
        [asyncCensor("batman")]
      )
    });

    // https://jsfiddle.net/Lbpxc0y7/1/

    // console.log(this.queryForm);

    const value$ = this.queryForm.get("query").valueChanges.pipe(
      // debounceTime(400),
      // distinctUntilChanged(),
      // filter(query => query.length >= 3)
    );

    const status$ = this.queryForm.statusChanges.pipe();
    const valid$ = status$.pipe(filter(status => status === "VALID"));

    const search$ = valid$.pipe(
      withLatestFrom(value$, (valid, value) => value)
    );

    search$.subscribe(query => this.search(query));
  }

  ngOnInit() {}

  @Output()
  queryChange = new EventEmitter();

  search(query) {
    // console.log(query);

    this.queryChange.emit(query);
  }
}
