import { Component, OnInit } from "@angular/core";
import { MusicService } from "../services/music.service";
import { ActivatedRoute, Router } from "@angular/router";
import { map, filter, tap } from "rxjs/operators";

@Component({
  selector: "app-music-search",
  templateUrl: "./music-search.component.html",
  styleUrls: ["./music-search.component.css"]
})
export class MusicSearchComponent implements OnInit {
  message: string;

  albums$ = this.service.getAlbums();
  query$ = this.service.queryChanges.pipe(
    tap(query =>
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: {
          q: query
        }
      })
    )
  );

  constructor(
    public service: MusicService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    route.queryParamMap
      .pipe(
        map(queryParamMap => queryParamMap.get("q")),
        filter(q => !!q)
      )
      .subscribe(q => this.search(q));
  }

  search(query) {
    this.service.search(query);
  }

  ngOnInit() {}
}
