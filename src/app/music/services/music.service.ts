import {
  Injectable,
  Inject,
  InjectionToken} from "@angular/core";
import { Album } from "src/app/model/Album";
import { HttpClient } from "@angular/common/http";
import { AlbumsResponse } from "../../model/Album";
import {
  map,
  switchMap,
  catchError,
  debounceTime,
  distinctUntilChanged,
  filter
} from "rxjs/operators";
import {
  BehaviorSubject,
  empty
} from "rxjs";

export const SEARCH_API_URL = new InjectionToken<string>(
  "Url for music search api"
);

@Injectable({
  providedIn: "root"
})
export class MusicService {
  albums$ = new BehaviorSubject<Album[]>([]);
  query$ = new BehaviorSubject<string>("batman");

  constructor(
    @Inject(SEARCH_API_URL)
    private api_url: string,
    private http: HttpClient
  ) {
    this.query$
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        filter(query => query.length >= 3),
        map(query => ({
          type: "album",
          q: query
        })),
        switchMap(params =>
          this.http
            .get<AlbumsResponse>(this.api_url, {
              params: params
            })
            .pipe(catchError(() => empty()))
        ),
        map(resp => resp.albums.items)
      )
      .subscribe(albums => this.albums$.next(albums));
  }

  /*  Inputs / Commands */
  search(query: string) {
    this.query$.next(query);
  }

  /* Outputs - Queries */
  queryChanges = this.query$.asObservable();

  getAlbums() {
    return this.albums$.asObservable();
  }

  ngOnDestroy(){
    // console.log('service says bye bye!')
  }
}
