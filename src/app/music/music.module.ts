import { NgModule, Injector } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicRoutingModule } from "./music-routing.module";
import { MusicSearchComponent } from "./music-search/music-search.component";
import { SearchFormComponent } from "./search-form/search-form.component";
import { AlbumsGridComponent } from "./albums-grid/albums-grid.component";
import { AlbumCardComponent } from "./album-card/album-card.component";
import { environment } from "../../environments/environment";
import { SEARCH_API_URL, MusicService } from "./services/music.service";
// import { HttpClientModule } from "@angular/common/http";

import { ReactiveFormsModule } from "@angular/forms";
import { MusicProviderDirective } from "./music-provider.directive";
import { SharedModule } from "../shared/shared.module";
// import { SecurityModule } from "../security/security.module";

@NgModule({
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumsGridComponent,
    AlbumCardComponent,
    MusicProviderDirective
  ],
  imports: [
    CommonModule,
    MusicRoutingModule,
    // HttpClientModule,
    ReactiveFormsModule,
    SharedModule
    // SecurityModule.forChild()
  ],
  exports: [
    /* MusicSearchComponent, MusicProviderDirective */
  ],
  providers: [
    {
      provide: SEARCH_API_URL,
      useValue: environment.search_api_url
    },
    // {
    //   provide: MusicService,
    //   useFactory(url, injector) {
    //     return new MusicService(url);
    //   },
    //   deps: [SEARCH_API_URL, Injector]
    // },
    // {
    //   provide: MusicService,
    //   useClass: MusicService,
    //   // deps: [SEARCH_API_URL]
    // },
    MusicService
  ]
})
export class MusicModule {}

export default MusicModule;
