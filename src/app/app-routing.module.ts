import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    redirectTo: "playlists",
    pathMatch: "full"
  },
  {
    path: "music",
    loadChildren: './music/music.module#MusicModule',
  },
  {
    path: "**",
    // matcher:()=>{},
    redirectTo: "playlists",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      // enableTracing: true
      // onSameUrlNavigation:'reload'
      // useHash: true
      // paramsInheritanceStrategy:'emptyOnly',
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
