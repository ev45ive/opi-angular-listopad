import { BrowserModule } from "@angular/platform-browser";
import { NgModule, Inject } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PlaylistsModule } from "./playlists/playlists.module";
import { SharedModule } from "./shared/shared.module";
import { MusicModule } from "./music/music.module";
import { SecurityModule } from "./security/security.module";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { environment } from "../environments/environment";
import { TestingComponent } from './testing/testing.component';

@NgModule({
  declarations: [AppComponent, TestingComponent],
  imports: [
    BrowserModule,
    PlaylistsModule,
    SharedModule,
    // MusicModule,
    HttpClientModule,
    SecurityModule.forRoot(environment.authConfig),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  // constructor(@Inject(HTTP_INTERCEPTORS) int:HTTP_INTERCEPTORS){
  //   console.log(int)
  // }
}
