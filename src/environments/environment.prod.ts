import { AuthConfig } from '../app/security/auth.service';
export const environment = {
  production: true,
  search_api_url: "https://api.spotify.com/v1/search",
  authConfig: {
    auth_url: "https://accounts.spotify.com/authorize",
    response_type: "token",
    redirect_uri: "http://localhost:4200/",
    client_id: "89d110ae02264df0a82a4d37910deebb"
  } as AuthConfig
};
